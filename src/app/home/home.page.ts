import { Component, ViewChild } from '@angular/core';
import { Slides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  slide_idx : number = 0;
  
  @ViewChild(Slides) slides: Slides;

  getIdx(){
    console.log("Move and action work!");
    this.slides.getActiveIndex().then( res => {
      this.slide_idx = res
      console.log('teste: ' , this.slide_idx);
    })
  }
}
