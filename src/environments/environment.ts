// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyALohhHfc0E33NC0MSUuHioNsmiWGEZD3E",
    authDomain: "banco-2235d.firebaseapp.com",
    databaseURL: "https://banco-2235d.firebaseio.com",
    projectId: "banco-2235d",
    storageBucket: "banco-2235d.appspot.com",
    messagingSenderId: "438192518473"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
